import React from 'react';
import { Box, Button, Divider, Fab, Theme, createStyles, withStyles } from '@material-ui/core';
import { Email, Facebook, Instagram, WhatsApp } from '@material-ui/icons';
import { RdsContainer, RdsText } from '@rdsystem/components';
import { email, facebook, instagram, whatsapp } from 'src/constants/links';

const RdsTextStyled = withStyles(() =>
  createStyles({
    root: {
      overflow: 'hidden',
      textOverflow: 'ellipsis'
    }
  })
)(RdsText);

const FabStyled = withStyles((theme: Theme) =>
  createStyles({
    root: {
      position: 'fixed',
      bottom: '16px',
      right: '16px',
      fontSize: '2rem',
      color: theme.palette.common.white,
      backgroundColor: theme.palette.success.main,
      '&:hover': {
        backgroundColor: theme.palette.success.dark,
        '@media (hover: none)': {
          backgroundColor: theme.palette.success.main
        }
      }
    }
  })
)(Fab);

const HomePage = () => {
  return (
    <RdsContainer maxWidth="xs">
      <RdsText align="center" margin="8px 0 12px" color="primary">
        Entre em contato comigo!
      </RdsText>

      <Box margin="16px 0 8px" display="flex" justifyContent="center">
        <Button onClick={() => window.open(whatsapp, '_blank')} style={{ textTransform: 'initial' }} fullWidth>
          <WhatsApp />
          <RdsText margin="0 0 0 8px">48 99904-8520</RdsText>
        </Button>
      </Box>

      <Box margin="0 0 8px" display="flex" justifyContent="center">
        <Button onClick={() => window.open(instagram, '_blank')} style={{ textTransform: 'initial' }} fullWidth>
          <Instagram />
          <RdsText margin="0 0 0 8px">@michelemsbr</RdsText>
        </Button>
      </Box>

      <Box margin="0 0 8px" display="flex" justifyContent="center">
        <Button onClick={() => window.open(facebook, '_blank')} style={{ textTransform: 'initial' }} fullWidth>
          <Facebook />
          <RdsText margin="0 0 0 8px">@michelemsbr</RdsText>
        </Button>
      </Box>

      <Box margin="0 0 16px" display="flex" justifyContent="center">
        <Button onClick={() => window.open(email, '_blank')} style={{ textTransform: 'initial' }} fullWidth>
          <Email />
          <RdsTextStyled margin="0 0 0 8px">michelevieiramd@gmail.com</RdsTextStyled>
        </Button>
      </Box>

      <Divider />

      <FabStyled onClick={() => window.open(whatsapp, '_blank')}>
        <WhatsApp fontSize="inherit" />
      </FabStyled>
    </RdsContainer>
  );
};

export default HomePage;
