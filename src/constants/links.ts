const whatsapp =
  'https://api.whatsapp.com/send?phone=5548999048520&text=Ol%C3%A1%2C+tudo+bem%3F+Gostaria+de+conhecer+melhor+seu+trabalho';

const phone = 'tel:+554832476564';

const instagram = 'https://www.instagram.com/michelemsbr/';

const facebook = 'https://www.facebook.com/michelemsbr/';

const email = 'mailto:michelevieiramd@gmail.com';

const address = 'https://goo.gl/maps/hrNdhtoe51vnzn1N6';

export { whatsapp, phone, instagram, facebook, email, address };
